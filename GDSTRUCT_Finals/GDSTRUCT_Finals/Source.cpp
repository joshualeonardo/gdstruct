#include <iostream>
#include "UnorderedArray.h"
#include "Stack.h"
#include "Queue.h"

using namespace std;

int main() {
	int size;

	cout << "Enter size for element sets: ";
	cin >> size;

	Queue<int> queue(size);
	Stack<int> stack(size);

	cout << "\n\n\n";
	while (true)
	{
		cout << "What do you want to do?" << endl;
		cout << "[1] - Push Elements" << endl;
		cout << "[2] - Pop Elements" << endl;
		cout << "[3] - Print everything then empty set" << endl;
		cout << "\nInput: ";

		int input;

		cin >> input;

		switch (input)
		{
		case 1:
			cout << "\n\n\nEnter Number: ";
			cin >> input;

			queue.push(input);
			stack.push(input);

			cout << "\n\nTop Element of Sets: " << endl;
			cout << "Queue Top: " << queue.top() << endl;
			cout << "Stack Top: " << stack.top() << endl;
			break;
		
		case 2:
			
			cout << "\n\n\nThe front elements have been popped!" << endl;

			queue.pop();
			stack.pop();

			if (queue.getSize() > 0 && stack.getSize() > 0)
			{
				cout << "\n\nTop Element of Sets: " << endl;
				cout << "Queue Top: " << queue.top() << endl;
				cout << "Stack Top: " << stack.top() << endl;
			}
			break;

		case 3:
			cout << "Queue Elements: " << endl;
			for (int i = 0; i < queue.getSize(); i++)
				cout << queue[i] << endl;


			cout << "Stack Elements: " << endl;
			for (int i = stack.getSize(); i > 0; i--)
				cout << stack[i - 1] << endl;

			queue.deleteSet();
			stack.deleteSet();
			break;

		}
		
		system("pause");
		system("cls");
	}

}