#include <iostream>
#include <string>
#include <time.h>
#include "UnorderedArray.h"

using namespace std;

//Creating dynamicArrays

void main()
{
	srand(time(NULL));
	int size = 0;


	cout << "What would you like the size of the array to be? " << endl;
	cout << "Size: ";
	cin >> size;

	UnorderedArray<int> grades(size);


	for (int i = 0; i < size; i++)
	{
		int randomRoll = rand() % 100 + 1;
		grades.push(randomRoll);
		cout << "[" << i << "] " << grades[i] << endl;
	}

	cout << "======================" << endl;
	int index = 0;
	cout << "What element would you like to remove? " << endl;
	cout << "Index: ";
	cin >> index;
	grades.remove(index);

	for (int i = 0; i < grades.getSize(); i++)
		cout << "[" << i << "] " << grades[i] << endl;

	cout << "======================" << endl;
	int value;
	cout << "Which value's index would you like to see? " << endl;
	cout << "Value : ";
	cin >> value;

	grades.search(value);

	system("pause");
}