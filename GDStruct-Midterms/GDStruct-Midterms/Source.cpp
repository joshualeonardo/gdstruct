#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>

using namespace std;

void main()
{
	srand(time(NULL));
	cout << "Enter size of array: ";
	int size;
	cin >> size;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}
	system("cls");
	while (true) {
		cout << "Unordered Array: ";
		for (int i = 0; i < unordered.getSize(); i++)
			cout << unordered[i] << "  ";
		cout << "\nOrdered Array: ";
		for (int i = 0; i < ordered.getSize(); i++)
			cout << ordered[i] << "  ";

		cout << " \n\nWhat would you like to do?" << endl;
		cout << "[1] Remove an Element    \n[2] Search for an Element    \n[3] Expand Array" << endl;
		int choice;
		cin >> choice;

		int input;
		if (choice == 1) {
			cout << "\nEnter Index to Remove: ";
			cin >> input;

			unordered.remove(input);
			ordered.remove(input);
			
			cout << "\nElement Removed: " << input << endl;

			cout << "\nUnordered Array: ";
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << "  ";
			cout << "\nOrdered Array: ";
			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << "  ";
			cout << endl;
		}

		else if (choice == 2) {
			cout << "\nEnter number to search: ";
			cin >> input;
			cout << endl;

			cout << "Unordered Array(Linear Search):\n";
			int result = unordered.linearSearch(input);
			if (result >= 0) {
				cout << "Linear Comparisons: " << result + 1 << endl;
				cout << input << " was found at Index " << result << ".\n";
			}	
			else 
				cout << input << " not found." << endl;

			int counter = 0;
			cout << "Ordered Array(Binary Search):\n";
			result = ordered.binarySearch(input, counter);
			if (result >= 0) {
				cout << "Binary Comparisons: " << counter << endl;
				cout << input << " was found at Index " << result << ".\n";
			}
			else
				cout << input << " not found." << endl;
		}

		else if (choice == 3) {
			cout << "\nInput size of expansion: ";
			cin >> input;

			for (int i = 0; i < input; i++) 
			{
				int rng = rand() % 101;
				unordered.push(rng);
				ordered.push(rng);
			}
		}
		system("pause");
		system("cls");
	}
}