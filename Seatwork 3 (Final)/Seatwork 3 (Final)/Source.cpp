#include <iostream>
#include <string>

using namespace std;

void computeSum(int num, int& sum)
{
	int digits = 0;
	if (num <= 0) {
		cout << "\b\b\b = " << sum << endl;
		return;
	}


	else if (num > 0) {
		digits = digits + num % 10;
		cout << digits << " + ";
		sum = sum + digits;
		num /= 10;
	}
	computeSum(num, sum);
}

void printfibonacciSequence(int value, int current, int previous)
{

	if (value <= 0)
		return;


	cout << current << " ";

	int x = current + previous;
	current = previous;
	previous = x;
	value--;

	printfibonacciSequence(value, current, previous);
}

int checkPrime(int input, int x = 2)
{
	if (input <= 2)
	{
		return (input == 2) ? true : false;
	}

	else if (input % x == 0)
	{
		return false;
	}

	else if (input % x != 0)
	{
		return true;
	}

	x++;
	return checkPrime(input);
}

void main()
{
	while (true)
	{

		int choice, input, sum = 0, current = 0, previous = 1;
		cout << "What would you like to do? " << endl;
		cout << "[1] Compute Sum   [2] Print Fibonacci Sequence  [3] Check number if Prime or not  [4] Quit" << endl;
		cin >> choice;
		if (choice == 1)
		{
			cout << "Give any number: ";
			cin >> input;

			computeSum(input, sum);
		}
		else if (choice == 2)
		{
			cout << "Give a limit to the Fibonacci Sequence: ";
			cin >> input;

			printfibonacciSequence(input, current, previous);
		}
		else if (choice == 3)
		{
			cout << "Give a number to check: ";
			cin >> input;

			if (input == 1 || input == 0)
				cout << input << " is a Special number" << endl;

			else if (checkPrime(input))
				cout << input << " is a Prime number" << endl;

			else
				cout << input << " is a Composite number" << endl;
		}
		else
		{
			break;
		}
		system("pause");
		system("cls");
	}
}